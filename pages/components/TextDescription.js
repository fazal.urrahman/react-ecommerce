import React from 'react'

export const TextDescription = ({title, d1, d2}) => {
  return (
    <div>
            <h3 className="text-3xl py-1 dark:text-white ">{title}</h3>
            <p className="text-md py-2 leading-8 text-gray-800 dark:text-gray-200">
             {d1}
            </p>
            <p className="text-md py-2 leading-8 text-gray-800 dark:text-gray-200">
              {d2}
            </p>
    </div>
  )
}
