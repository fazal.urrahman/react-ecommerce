import React from 'react';
import Image from 'next/image';
import code from "../../public/code.png";
import design from "../../public/design.png";
import consulting from "../../public/consulting.png";




export const CardCustom = ({imagedes,title,desc,data_title,data}) => {
  return (
    <div className="text-center shadow-lg p-10 rounded-xl my-10  dark:bg-white flex-1">
    <Image src={imagedes == 1 ? design: imagedes == 2 ?consulting: imagedes == 3 ?code: code} width={100} height={100} />
    <h3 className="text-lg font-medium pt-8 pb-2  ">
      {title}
    </h3>
    <p className="py-2">
     {desc}
    </p>
    <h4 className="py-4 text-teal-600">{data_title}</h4>
    { data.map(function(value, index){
       return (<p className="text-gray-800 py-1">{value}</p>) })  
        }
    
   
  </div>
  )
}
