import React from 'react'
import Image from 'next/image';
import Avatar from '../../public/spacecat2.jpeg'
import {
  AiFillTwitterCircle,
  AiFillLinkedin,
  AiFillYoutube,
} from "react-icons/ai";
export const Intro = ({name,role,description}) => {
  return (
    <div className="text-center p-10 py-10">
    <h2 className="text-5xl py-2 text-red-500 font-medium dark:text-teal-400 md:text-6xl ">
      {name}
    </h2>
    <h3 className="text-2xl py-2 dark:text-white md:text-3xl">
      {role}
    </h3>
    <p className="text-md py-5 leading-8 text-gray-800 dark:text-gray-200 max-w-xl mx-auto md:text-xl">
     {description}
    </p>
    <div className="text-5xl flex justify-center gap-16 py-3 text-gray-600 dark:text-gray-400">
      <AiFillTwitterCircle />
      <AiFillLinkedin />
      <AiFillYoutube />
    </div>
    <div className="mx-auto bg-gradient-to-b from-teal-500 rounded-full w-80 h-80 relative overflow-hidden mt-20 md:h-96 md:w-96">
      <Image src={Avatar} layout="fill" objectFit="cover" />
    </div>
  </div>
  )
}
