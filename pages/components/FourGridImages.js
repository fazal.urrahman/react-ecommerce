import React from 'react'
import Image from "next/image";
import cat1 from "../../public/spacecat1.jpeg";
import cat2 from "../../public/spacecat4.jpeg";
import cat3 from "../../public/spacecat3.jpeg";
import cat4 from "../../public/spacecat5.jpeg";

export const FourGridImages = () => {
  return (
    <div className="flex flex-col gap-10 py-10 lg:flex-row lg:flex-wrap">
            <div className="basis-1/12 flex-1 ">
              <Image
                className="rounded-lg object-cover"
                width={"100%"}
                height={"100%"}
                layout="responsive"
                src={cat3}
              />
            </div>
            <div className="basis-1/12 flex-1">
              <Image
                className="rounded-lg object-cover"
                width={"100%"}
                height={"100%"}
                layout="responsive"
                src={cat4}
              />
            </div>
            <div className="basis-1/12 flex-1">
              <Image
                className="rounded-lg object-cover"
                width={"100%"}
                height={"100%"}
                layout="responsive"
                src={cat2}
              />
            </div>
            <div className="basis-1/12 flex-1">
              <Image
                className="rounded-lg object-cover"
                width={"100%"}
                height={"100%"}
                layout="responsive"
                src={cat1}
              />
            </div>
          </div>
  )
}
