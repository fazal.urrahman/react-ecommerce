import React from 'react'
import Image from "next/image";

export const ImageComponet = (imagesrc) => {
  return (
    <div>
    <Image
    className="rounded-lg object-cover"
                width={"100%"}
                height={"100%"}
                layout="responsive"
                src={imagesrc}
              />
              </div>
  )
}
